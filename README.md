# Kafka with PostgreSQL and MicroStrategy

## Objectives

* Creation & loading datamart containing Berlin weather data** (sun, rain, temperature)
and Github commits for an open sources project (commits over time) over the last 3
months.
    - Load data from github via API using Kafka and load in a Postgres DB.
    - Extract relevant weather data from a publicly available sources and store them in a Postgres DB.
    - Define a datamart with number of commits per day. Create a schema for DWH accordingly.

* Use the MicroStrategy desktop client to visualize your datamart nicely as a dashboard within MicroStrategy.

### Solution
* Creation of two Kafka Topics running in parallel with different task settings (tasks.py):
    - Github: 
        - GithubProducer (producer.py): 
            - It generates a list of commits since <NUMBER_OF_DAYS in config.py> days before using Github API.
            - The commits data are transformed to a particular schema and validated accordingly using jsonschema (see config.py: COMMIT_SCHEMA).
            - The frequency of data generation depends on <GITHUB_TASK_TIME_PERIOD in config.py>.
        - GithubConsumer (consumer.py):
            - The message value received is saved in a file github.json.
            - The TableDDL class in table.py is used for schema(github) and table(github) creation.
            - The message value is loaded to a table in the database.
            - Creates a data mart containing commit_count per day.

    - Weather:
        - WeatherProducer (producer.py):
            - It fetches weather information(temp, pressure, humidity) every <WEATHER_TASK_TIME_PERIOD in config.py> using https://openweathermap.org/api (historical data could not be fetched because the api was not free). 
            - The weather data are transformed to <WEATHER_SCHEMA in config.py> and validated using jsonschema.
        - WeatherConsumer (consumer.py):
            - The message value is saved in a file named weather.json.
            - The TableDDL class in table.py is used for schema(weather) and table(weather) creation.
            - The message value is loaded to a table in the database.

## Requirements

* Kafka
    - Mac
    ```
    $ brew install kafka # Mac + Java 1.8+ is required
    ```
    - Unix systems
    ```
    $ tar -xzf kafka_2.11-0.11.0.0.tgz # for Unix systems
    ```
    
    ```
    $ cd kafka_2.11-0.11.0.0
    ```
    
* PostgreSQL
* MicroStategy (https://www.microstrategy.com/us/desktop)
* Python >= 2.7 or Python 3.x
* librdkafka >= 0.9.1
    - Mac
    ```
    $ sudo brew install librdkafka # Mac
    ```
    
    - Debian/Ubuntu
    ```
    $ sudo apt-get install librdkafka-dev python-dev # Debian/Ubuntu
    ```
    
## Steps to follow:

* Start the ZooKeeper and Kafka server
    - For Mac users
    ```
    $ zookeeper-server-start /usr/local/etc/kafka/zookeeper.properties & kafka-server-start /usr/local/etc/kafka/server.properties
    ```
    - For others
    ```
    $ bin/zookeeper-server-start.sh config/zookeeper.properties & bin/kafka-server-start.sh config/server.properties
    ```
    
* Create Python3 virtual environment and activate it.
* Install python modules from the requirements.txt file
    ``` 
    $ pip install -r requirements.txt
    ```
    
* Creata a PostgreSQL database and add the settings of the database to config.py
* Look at the settings in config.py. Change them as needed.
* Run in terminal: 
    ```
    $ python producer.py & python consumer.py
    ```
    
* Open MicroStrategy desktop client and connect it to the database for the dashboard.
