# Frequency at which data are needed to be updated (in seconds)
GITHUB_TASK_TIME_PERIOD = 3600 * 24  # every day
WEATHER_TASK_TIME_PERIOD = 3600  # every hour

# Number of days for which the data are needed to be fetched.
NUMBER_OF_DAYS = 90

# Database configuration (local settings used, with database name: kafka)
DATABASE = {
    "host": "localhost",
    "user": "postgres",
    "password": "postgres",
    "database": "kafka",
    "port": 5432
}


WEATHER_API_KEY = "2ce9c365d26abca5a7cac0b6f45e4b2b"

# Validating commit json
COMMIT_SCHEMA = {
    "type": "object",
    "properties": {
        "sha": {"type": "string"},
        "name": {"type": "string"},
        "date": {"type": "string"},
        "message": {"type": "string"},
        "email": {"type": "string"}
    }
}


# Validating commit json
WEATHER_SCHEMA = {
    "type": "object",
    "properties": {
        "dt": {"type": "string"},
        "temp": {"type": "number"},
        "pressure": {"type": "integer"},
        "humidity": {"type": "integer"},
    }
}

# Table columns for the commit table.
COMMIT_TABLE = {
    "sha": "VARCHAR(100) PRIMARY KEY",
    "name": "VARCHAR(255) NOT NULL",
    "email": "VARCHAR(255) NOT NULL",
    "date": "TIMESTAMP NOT NULL",
    "message": "TEXT NOT NULL"
}


# Table columns for the table with aggregation of commits.
AGGREGATED_COMMIT_TABLE = {
    "date": "TIMESTAMP PRIMARY KEY",
    "number_of_commits": "INT NOT NULL"
}


# Table columns for the weather table.
WEATHER_TABLE = {
    "dt": "TIMESTAMP NOT NULL PRIMARY KEY",
    "temp": "DECIMAL(5,2) NOT NULL",
    "pressure": "INT NOT NULL",
    "humidity": "INT NOT NULL"
}
