import datetime
import json
import psycopg2


class TableDLL:
    def __init__(self, schema, table_name, table_config, table_schema, data):
        self.date = datetime.date.today()
        self.schema = schema
        self.table_name = table_name
        self.table_config = table_config
        self.table_schema = table_schema
        self.data = data

    def create_schema(self):
        """"query for: Creates a Schema"""
        return "CREATE SCHEMA IF NOT EXISTS {};".format(self.schema)

    def create_table(self):
        """Creates table."""
        query = "CREATE TABLE IF NOT EXISTS {schema}.{table} ({columns});".format(
            schema=self.schema,
            table=self.table_name,
            columns=",".join(["{} {}".format(key, value) for key, value in
                              self.table_schema.items()])
        )
        return query

    def create_view(self, number_of_days=90):
        """Creates a view which takes union of all <number_of_days> tables
        for last 90 days. The tables can be created for each day so that
        new data for the next day can be stored as a separate table and old
        tables can be deleted.
        """
        dates = [(datetime.date.today() - datetime.timedelta(i + 1)).isoformat()
                 for i in range(number_of_days)]
        tables = ["{}_{}".format(self.table_name, date) for date in dates]
        select_stmt_list = ['SELECT * FROM "{}"."{}"\n'.format(self.schema, table)
                            for table in tables]
        select_stmt = ' UNION ALL '.join(select_stmt_list)
        view_name = "all_days_{}".format(self.table_name)
        view_stmt = "CREATE OR REPLACE VIEW {} AS (\n{});".format(view_name,
                                                                  select_stmt)
        return view_stmt

    def write_to_file(self):
        """Writes the data to file."""
        with open('{}.json'.format(self.table_name), 'w') as outfile:
            json.dump(self.data, outfile, sort_keys=True, indent=4,
                      ensure_ascii=False)

    def insert_data(self):
        """query for: Inserts data to the table"""
        query = "INSERT INTO {schema}.{table} SELECT * from json_populate_recordset" \
                "(null::{schema}.{table}, '{data}') ON CONFLICT ON CONSTRAINT " \
                "{table}_pkey DO NOTHING;".format(
                    schema=self.schema,
                    table=self.table_name,
                    data=json.dumps(self.data, ensure_ascii=False)
        )
        return query

    @staticmethod
    def delete_old_table(schema, table):
        """query for: Deletes the table."""
        return "DROP TABLE IF EXISTS {schema}.{table};".format(
            schema=schema, table=table)

    def create_data_mart(self, table_name, table_schema):
        """query for: Creates a separate table for aggregated data based on
        date column"""
        delete_query = self.delete_old_table(self.schema, table_name)
        create_query = "CREATE TABLE IF NOT EXISTS {schema}.{table} ({columns});".format(
            schema=self.schema,
            table=table_name,
            columns=",".join(["{} {}".format(key, value) for key, value in
                              table_schema.items()])
        )
        aggregate_query = "SELECT DATE(date) date, COUNT(*) totalCount " \
                          "FROM {schema}.{table} GROUP BY DATE(date)".\
            format(table=self.table_name, schema=self.schema)
        query = "INSERT INTO {schema}.{table} ({columns}) {aggregate_query};".format(
            schema=self.schema,
            table=table_name,
            aggregate_query=aggregate_query,
            columns=",".join(table_schema.keys())
        )
        conn = psycopg2.connect(host=self.table_config['host'],
                                user=self.table_config['user'],
                                password=self.table_config['password'],
                                database=self.table_config['database'],
                                port=self.table_config['port'])
        cursor = conn.cursor()
        cursor.execute(delete_query)
        cursor = conn.cursor()
        cursor.execute(create_query)
        conn.commit()
        cursor.execute(query)
        conn.commit()
        conn.commit()
        conn.close()

    def run(self):
        """step by step process"""
        self.write_to_file()
        conn = psycopg2.connect(host=self.table_config['host'],
                                user=self.table_config['user'],
                                password=self.table_config['password'],
                                database=self.table_config['database'],
                                port=self.table_config['port'])
        cursor = conn.cursor()
        cursor.execute(self.create_schema())
        conn.commit()
        cursor.execute(self.create_table())
        conn.commit()
        cursor.execute(self.insert_data())
        conn.commit()
        conn.close()
