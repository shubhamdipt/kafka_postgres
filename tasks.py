import datetime
import requests
from jsonschema import validate
from config import COMMIT_SCHEMA, WEATHER_SCHEMA, WEATHER_API_KEY


class GithubAPI:
    """Class containing the methods related to Github API."""

    def __init__(self, number_of_days):
        self.number_of_days = number_of_days

    @staticmethod
    def clean_commits(results):
        """Returns a dictionary of relevant information regarding commits."""
        all_commits = []
        for commit in results:
            try:
                data = {
                    "sha": commit["sha"],
                    "name": commit["commit"]["committer"]["name"],
                    "email": commit["commit"]["committer"]["email"],
                    "date": commit["commit"]["committer"]["date"],
                    "message": commit["commit"]["message"].replace("'", "''")  # to remove single quotations
                }
                validate(data, COMMIT_SCHEMA)
                all_commits.append(data)
            except Exception as e:
                # print(str(e))
                continue
        return all_commits

    def get_github_commits(self):
        """Gets a list of commits using Github API."""
        days_before = (datetime.date.today() - datetime.timedelta(
            self.number_of_days)).isoformat()
        page = 1
        len_results = 100
        total_result = []
        while len_results == 100:
            url = 'https://api.github.com/repos/django/django/commits' \
                  '?per_page=100&page={page}&since={since}T00:00:01Z'.format(
                    since=days_before,
                    page=page
            )
            try:
                data = requests.get(url).json()
                if 'API rate limit exceeded' in data['message']:
                    print('Github API rate limit exceeded')
                    break
            except Exception as e:
                # ToDo
                # print(str(e))
                pass
            len_results = len(data)
            total_result += data
            page += 1
        return self.clean_commits(total_result)


class WeatherAPI:
    """Class containing the methods related to Weather API."""

    def __init__(self, number_of_days):
        self.number_of_days = number_of_days

    @staticmethod
    def clean_weather_data(result):
        """Returns a dictionary of relevant information regarding weather."""
        clean_data = []
        try:
            data = {
                "dt": datetime.datetime.fromtimestamp(int(result["dt"])).isoformat(),
                "temp": result["main"]["temp"],
                "pressure": result["main"]["pressure"],
                "humidity": result["main"]["humidity"]
            }
            validate(data, WEATHER_SCHEMA)
            clean_data.append(data)
        except Exception as e:
            # ToDo
            # print(str(e))
            pass
        return clean_data

    def get_weather_data(self):
        """Gets current weather data for Berlin."""
        data = {}
        url = "http://api.openweathermap.org/data/2.5/weather?q=Berlin&" \
              "appid={api_key}".format(api_key=WEATHER_API_KEY)
        try:
            data = requests.get(url).json()
        except Exception as e:
            # ToDo
            # print(str(e))
            pass
        return self.clean_weather_data(data)
