from kafka import KafkaConsumer
from table import TableDLL
from config import (
    DATABASE,
    COMMIT_TABLE,
    WEATHER_TABLE,
    AGGREGATED_COMMIT_TABLE,
    GITHUB_TASK_TIME_PERIOD,
    WEATHER_TASK_TIME_PERIOD
)
import json
import time
import multiprocessing


class GithubConsumer(multiprocessing.Process):
    daemon = True

    def run(self):

        consumer = KafkaConsumer(bootstrap_servers='localhost:9092',
                                 value_deserializer=lambda m: json.loads(
                                     m.decode('ascii')))
        consumer.subscribe(['github'])

        for message in consumer:
            print("Github message received.")
            print("Next Github message to arrive in {} seconds...".format(
                GITHUB_TASK_TIME_PERIOD
            ))
            table = TableDLL(
                schema='github',
                table_name='github',
                table_config=DATABASE,
                data=message.value,
                table_schema=COMMIT_TABLE
            )
            table.run()
            # Creates a data mart with commit count per day
            table.create_data_mart(
                table_name='commit_count',
                table_schema=AGGREGATED_COMMIT_TABLE,
            )


class WeatherConsumer(multiprocessing.Process):
    daemon = True

    def run(self):
        consumer = KafkaConsumer(bootstrap_servers='localhost:9092',
                                 value_deserializer=lambda m: json.loads(
                                     m.decode('ascii')))
        consumer.subscribe(['weather'])

        for message in consumer:
            print("Weather message received.")
            print("Next Weather message to arrive in {} seconds...".format(
                WEATHER_TASK_TIME_PERIOD
            ))
            table = TableDLL(
                schema='weather',
                table_name='weather',
                table_config=DATABASE,
                data=message.value,
                table_schema=WEATHER_TABLE
            )
            table.run()


tasks = [
    GithubConsumer(),
    WeatherConsumer()
]

for t in tasks:
    t.start()

# time.sleep(10)
while True:
    pass
