from kafka import KafkaProducer
from tasks import GithubAPI, WeatherAPI
from config import (
    NUMBER_OF_DAYS,
    GITHUB_TASK_TIME_PERIOD,
    WEATHER_TASK_TIME_PERIOD
)
import time
import json
import threading
import logging


class GithubProducer(threading.Thread):
    daemon = True

    def run(self):

        producer = KafkaProducer(bootstrap_servers='localhost:9092',
                                 value_serializer=lambda v: json.dumps(
                                     v).encode('utf-8'))

        while True:
            github = GithubAPI(NUMBER_OF_DAYS)
            github_data = github.get_github_commits()
            if github_data:
                producer.send('github', github_data)
            time.sleep(GITHUB_TASK_TIME_PERIOD)


class WeatherProducer(threading.Thread):
    daemon = True

    def run(self):

        producer = KafkaProducer(bootstrap_servers='localhost:9092',
                                 value_serializer=lambda v: json.dumps(
                                     v).encode('utf-8'))

        while True:
            weather = WeatherAPI(NUMBER_OF_DAYS)
            weather_data = weather.get_weather_data()
            if weather_data:
                producer.send('weather', weather_data)
            time.sleep(WEATHER_TASK_TIME_PERIOD)


tasks = [
    GithubProducer(),
    WeatherProducer()
]

for t in tasks:
    t.start()

while True:
    pass
# time.sleep(10)
